"" Derecho's .vimrc

"" Settings required before loading plugins
set nocompatible                    " choose no compatibility with legacy vi
let g:ale_completion_enabled = 1    " use ALE as the completion engine

"" Load plugins
call plug#begin()
Plug 'nanotech/jellybeans.vim'
Plug 'LaTeX-Box-Team/LaTeX-Box'
Plug 'scrooloose/nerdtree'
Plug 'vim-scripts/ShowMarks'
Plug 'ervandew/supertab'
Plug 'davidoc/taskpaper.vim'
Plug 'derekwyatt/vim-fswitch'
Plug 'henrik/vim-indexed-search'
Plug 'christoomey/vim-tmux-navigator'
Plug 'thaerkh/vim-indentguides'
Plug 'momota/cisco.vim'
Plug 'dense-analysis/ale'
Plug 'sheerun/vim-polyglot'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'mhinz/vim-signify'
call plug#end()

"" General
syntax enable                       " enable syntax highlighting
set encoding=utf-8                  " use utf-8 by default
set showcmd                         " display incomplete commands
filetype plugin indent on           " load file type plugins + indentation
set number                          " enable line numbering
set ruler                           " show current row + column
set colorcolumn=80                  " make column 80 stand out
set wildignorecase                  " ignore case when tab-completing filenames
set wildmode=longest,list,full      " more intuitive tab-completion
"" Whitespace
set nowrap                          " don't wrap lines
set tabstop=4 shiftwidth=4          " a tab is four spaces
set expandtab                       " use spaces, not tabs (optional)
set backspace=indent,eol,start      " backspace through everything in insert mode
"" Searching
set hlsearch                        " highlight matches
set incsearch                       " incremental searching
set ignorecase                      " searches are case insensitive...
set smartcase                       " ... unless they contain at least one capital letter

"" Folding
set foldmethod=syntax               " enable folding by syntax
let javaScript_fold=1               " JavaScript
let perl_fold=1                     " Perl
let php_folding=1                   " PHP
let r_syntax_folding=1              " R
let ruby_fold=1                     " Ruby
let sh_fold_enabled=1               " sh
let vimsyn_folding='af'             " Vim script
let xml_syntax_folding=1            " XML
let tex_fold_enabled=1              " enable TeX folding (latex-box)
let g:markdown_folding = 1          " enable undocumented Markdown folding
" Python has no folding rules, fold on indentation
autocmd FileType python setlocal foldmethod=indent
" Do not autounfold while in insert mode
autocmd InsertEnter * let w:last_fdm=&foldmethod | setlocal foldmethod=manual
autocmd InsertLeave * let &l:foldmethod=w:last_fdm
set fdo-=block                      " stop block navigation from opening folds
set fdo-=hor                        " stop horizontal navigation from opening folds
set fdo-=search                     " stop search from opening folds

"" Look and feel
set diffopt=vertical,filler         " split windows vertically and keep text synchronized
set splitbelow                      " place completion window below rather than above current window
" Work in the vertical center of the window
execute "set scrolloff=" . winheight(0)/4
au WinEnter,WinLeave,VimResized * execute "set scrolloff=" . winheight(0)/4
" Override background colours in theme (transparency fix)
autocmd ColorScheme * highlight Normal ctermbg=None
autocmd ColorScheme * highlight NonText ctermbg=None
colo jellybeans                     " apply jellybeans theme (after transparency fix!)
" Let colorcolumn use the same colours as the line numbering column
hi! link ColorColumn LineNr
" Only show signs for marks that have been manually placed
let showmarks_include = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
" Change sign colours
hi ShowMarksHLl ctermfg=233 ctermbg=59
hi ShowMarksHLu ctermfg=233 ctermbg=59
hi! link SignColumn LineNr
" Don't show EOL symbols when indentguides is used
let g:indentguides_toggleListMode=0
" Don't conceal JSON doublequotes
let g:indentguides_ignorelist = ['json', 'markdown']
" Custom always-on statusline
set statusline+=%r                  " Readonly flag
set statusline+=%m                  " Modified flag
set statusline+=%f                  " File path (relative to pwd)
set statusline+=\ %y                " Filetype/syntax
set statusline+=%{sy#repo#get_stats_decorated()} " Version control stats
set statusline+=%=                  " Right aligned items from now on
set statusline+=%p%%                " Progress through document as a percentage
set statusline+=\ %l,               " Current line
set statusline+=%c                  " Current column
set laststatus=2

"" Plugin or use-case specific settings and commands
" Completion
set omnifunc=ale#completion#OmniFunc
let g:ale_completion_autoimport=1
" Automatic source/header opening with :FS
autocmd BufEnter *.c,*.cpp command! FS :FSSplitLeft
autocmd BufEnter *.h command! FS :FSSplitRight
let g:fsnonewfiles=1                " do not create a file if no match is found
" LaTeX
let LatexBox_latexmk_preview_continuously=1
let tex_comment_nospell=1
let tex_flavor='latex'
autocmd FileType tex set spell
autocmd FileType tex setlocal spell spelllang=en_gb

"" Custom keybinds
" Make home button go to first non-whitespace character on line
noremap <Home> ^
inoremap <Home> <ESC>I
" Use shift + direction to change tabs
noremap <S-l> gt
noremap <S-h> gT
" ALE (linting + completion)
nmap <Leader>h <Plug>(ale_hover)
nmap <Leader>r <Plug>(ale_find_references)
nmap <Leader>d <Plug>(ale_go_to_definition)
nmap <Leader>D <Plug>(ale_go_to_definition_in_tab)
nmap <Leader>R <Plug>(ale_rename)
" fzf (fuzzy finder)
noremap <Leader>ff :Files<cr>
noremap <Leader>fgf :GFiles<cr>
noremap <Leader>fgs :GFiles?<cr>
noremap <Leader>fgc :Commits<cr>
noremap <Leader>fl :Lines<cr>
noremap <Leader>fc :History:<cr>
noremap <Leader>fs :History/<cr>
" Signify (version control)
noremap <Leader>sd :SignifyDiff<cr>
noremap <Leader>su :SignifyHunkUndo<cr>

"" Extra commands
" Pa - paste buffer to snippt
command! Pa :w !curl -F 'paste=<-' http://s.drk.sc | sed 's/$/?guess/g'
" w!! - save as root
cmap w!! %!sudo tee 2> /dev/null %

"" Load additional local configuration
source ${MYVIMRC}-local
