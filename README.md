# Dotfiles
Some of my dotfiles.
Instructions below are present as a note to myself.

## Deployment
Issue the following command to deploy the dotfiles to a machine:

    git clone https://github.com/Derecho/Dotfiles.git
    cd Dotfiles
    git submodule update --init

In `vim` issue:

    :PluginInstall

If you use i3, issue:

    .i3/blocks/ovpn install

Now checkout the specific branch for this machine with any local configuration, or create one.
The following files may need adjusting in a new branch:

   - .vimrc-local
   - .Xresources
   - .i3/config

## Branches
Since the machines I use the dotfiles on deviate from each other, some configuration options will clash with each other.
To remedy this situation, each machine gets its own branch:

   - master
   - desktop
   - laptop
   - work

The master branch will have the common configuration and some template files/values.

## Dependencies
Some of these configurations depend on extra utilities that should be present on the system.

## vim
   - ag.vim  
       Depends on `ag` (the Silver Searcher).
   - LaTeX-Box  
       Depends on a working texlive environment along with `latexmk`.
   - ALE  
       Depends on suitable language servers and linters, i.e. `ccls` for C/C++

## i3
  - Application launcher and window selector
  -   Depends on `rofi`.
  - Status bar
      Depends on `i3blocks`.
      The temperature block depends on PySensors.
  - Volume management  
      Depends on `amixer`.
  - Backlight management  
      Depends on `xbacklight`.
  - Quake-style console  
      Depends on `yeahconsole`.
  - Wallpaper  
      Depends on `feh`, which is an image viewer.
  - Auto-hiding of mouse pointer  
      Depends on `unclutter`.
  - Showing current keyboard layout  
      Depends on `xkblayout-state`
