#!/bin/bash

WALLPAPERDIR="$HOME/storage/Wallpapers"

while true; do
    find $WALLPAPERDIR -maxdepth 1 -type f \( -name '*.jpg' -o -name '*.png' \) -print0 | shuf -n1 -z | xargs -0 feh --bg-fill
    sleep 10m
done
