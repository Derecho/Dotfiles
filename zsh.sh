# This file gets sourced by ~/.vimrc, after all the oh-my-zsh things and local
# settings have been applied.

# Put ZSH in vi mode instead of default emacs
bindkey -v
bindkey "^?" backward-delete-char  # Vim backspace, not vi
export KEYTIMEOUT=1   # Lower ESC delay
bindkey -sM vicmd '^[' ''  # Force start of the above timeout on a mere ESC

# Show whether we are in normal or instert mode
function zle-line-init zle-keymap-select {
	INSERT_MODE_PROMPT="%{$fg_bold[white]%}-- INSERT --%{$reset_color%}"
    RPS1="${${KEYMAP/vicmd/}/(main|viins)/$INSERT_MODE_PROMPT}"
    zle reset-prompt
}
zle -N zle-line-init
zle -N zle-keymap-select
setopt transientrprompt  # Don't keep the right prompt after a command
